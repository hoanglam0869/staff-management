function showMessage(id, message) {
  document.getElementById(id).style.display = "block";
  document.getElementById(id).innerText = message;
}

function isValidAccount(account, isAdd, employeeList, id) {
  var regex = /^\d{4,6}$/g;
  var index = employeeList.findIndex(function (employee) {
    return account == employee.account;
  });
  if (index != -1 && isAdd) {
    showMessage(id, "Tài khoản bị trùng");
    return false;
  } else if (regex.test(account)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Tài khoản tối đa 4 - 6 ký số");
    return false;
  }
}

function isValidName(name, id) {
  var regex = /^[a-zA-Z]{1,}(?: [a-zA-Z]+){1,}$/g;
  if (regex.test(name)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Tên nhân viên phải là chữ");
    return false;
  }
}

function isValidEmail(email, id) {
  var regex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regex.test(email)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Email phải đúng định dạng");
    return false;
  }
}

function isValidPassword(password, id) {
  var regex =
    /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–\[{}\]:;',?\/*~$^+=<>]).{8,20}$/g;
  if (regex.test(password)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(
      id,
      "Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
    return false;
  }
}

function isValidWorkDate(workDate, id) {
  var regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/g;
  if (regex.test(workDate)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Ngày làm định dạng mm/dd/yyyy");
    return false;
  }
}

function isValidBasicSalary(basicSalary, id) {
  var regex = /^[1-9]\d{6}$|^1\d{7}$|^20{7}$/g;
  if (regex.test(basicSalary)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Lương cơ bản 1 000 000 - 20 000 000");
    return false;
  }
}

function isValidPosition(position, id) {
  var regex = /Sếp|Trưởng phòng|Nhân viên/g;
  if (regex.test(position)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(
      id,
      "Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)"
    );
    return false;
  }
}

function isValidWorkHours(workHours, id) {
  var regex = /^[89]\d$|^1\d{2}$|^200$/g;
  if (regex.test(workHours)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Số giờ làm trong tháng 80 - 200 giờ");
    return false;
  }
}
