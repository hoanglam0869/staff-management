function getFormData() {
  var account = document.getElementById("tknv").value * 1;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var workDate = document.getElementById("datepicker").value;
  var basicSalary = document.getElementById("luongCB").value * 1;
  var position = document.getElementById("chucvu").value;
  var workHours = document.getElementById("gioLam").value * 1;

  return new Employee(
    account,
    name,
    email,
    password,
    workDate,
    basicSalary,
    position,
    workHours
  );
}

function renderTable(employeeList) {
  document.getElementById("tableDanhSach").innerHTML = employeeList
    .map(function (employee) {
      return `<tr>
      <td>${employee.account}</td>
      <td>${employee.name}</td>
      <td>${employee.email}</td>
      <td>${employee.workDate}</td>
      <td>${employee.position}</td>
      <td>${new Intl.NumberFormat().format(employee.totalSalary())}</td>
      <td>${employee.employeeType()}</td>
      <td>
        <button class="btn btn-danger" onclick="deleteEmployee(${
          employee.account
        })"><i class="fa fa-trash"></i></button>
        <button class="btn btn-warning" onclick="editEmployee(${
          employee.account
        })"><i class="fa fa-edit"></i></button>
      </td>
    </tr>`;
    })
    .join("");
}

function setFormData(employee) {
  document.getElementById("tknv").value = employee.account;
  document.getElementById("name").value = employee.name;
  document.getElementById("email").value = employee.email;
  document.getElementById("password").value = employee.password;
  document.getElementById("luongCB").value = employee.basicSalary;
  document.getElementById("chucvu").value = employee.position;
  document.getElementById("gioLam").value = employee.workHours;
}
