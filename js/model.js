function Employee(
  account,
  name,
  email,
  password,
  workDate,
  basicSalary,
  position,
  workHours
) {
  this.account = account;
  this.name = name;
  this.email = email;
  this.password = password;
  this.workDate = workDate;
  this.basicSalary = basicSalary;
  this.position = position;
  this.workHours = workHours;
  this.totalSalary = function () {
    if (this.position == "Sếp") {
      return this.basicSalary * 3;
    } else if (this.position == "Trưởng phòng") {
      return this.basicSalary * 2;
    } else {
      return this.basicSalary * 1;
    }
  };
  this.employeeType = function () {
    if (this.workHours >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.workHours >= 176) {
      return "Nhân viên giỏi";
    } else if (this.workHours >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
