var employeeList = [];

const EMPLOYEE_LIST_LOCAL = "EMPLOYEE_LIST_LOCAL";

var jsonData = localStorage.getItem(EMPLOYEE_LIST_LOCAL);
if (jsonData != null) {
  employeeList = JSON.parse(jsonData).map(function (employee) {
    return new Employee(
      employee.account,
      employee.name,
      employee.email,
      employee.password,
      employee.workDate,
      employee.basicSalary,
      employee.position,
      employee.workHours
    );
  });
  renderTable(employeeList);
}

function addEmployee() {
  var employee = getFormData();
  var isValid = checkValid(employee, true);
  if (isValid) {
    employeeList.push(employee);
    renderTable(employeeList);
    saveData();
  }
}

function saveData() {
  var dataJSON = JSON.stringify(employeeList);
  localStorage.setItem(EMPLOYEE_LIST_LOCAL, dataJSON);
}

function deleteEmployee(account) {
  var index = employeeList.findIndex(function (employee) {
    return employee.account == account;
  });
  if (index != -1) {
    employeeList.splice(index, 1);
    renderTable(employeeList);
    saveData();
  }
}

function editEmployee(account) {
  var index = employeeList.findIndex(function (employee) {
    return employee.account == account;
  });
  if (index != -1) {
    document.getElementById("btnThem").click();
    var employee = employeeList[index];
    setFormData(employee);
  }
}

function updateEmployee() {
  var employee = getFormData();
  var index = employeeList.findIndex(function (item) {
    return item.account == employee.account;
  });
  var isValid = checkValid(employee, false);
  if (index != -1 && isValid) {
    employeeList[index] = employee;
    renderTable(employeeList);
    saveData();
  }
}

function checkValid(employee, isAdd) {
  // check if account is valid
  var isValid = isValidAccount(employee.account, isAdd, employeeList, "tbTKNV");
  // check if name is valid
  isValid = isValid & isValidName(employee.name, "tbTen");
  // check if email is valid
  isValid = isValid & isValidEmail(employee.email, "tbEmail");
  // check if password is valid
  isValid = isValid & isValidPassword(employee.password, "tbMatKhau");
  // check if work date is valid
  isValid = isValid & isValidWorkDate(employee.workDate, "tbNgay");
  // check if basic salary is valid
  isValid = isValid & isValidBasicSalary(employee.basicSalary, "tbLuongCB");
  // check if position is valid
  isValid = isValid & isValidPosition(employee.position, "tbChucVu");
  // check if work hours is valid
  isValid = isValid & isValidWorkHours(employee.workHours, "tbGiolam");
  return isValid;
}

document.getElementById("btnTimNV").onclick = function () {
  var key = document.getElementById("searchName").value;
  var filterList = employeeList.filter(function (employee) {
    return (
      employee.employeeType().toUpperCase().indexOf(key.toUpperCase()) != -1
    );
  });
  renderTable(filterList);
};

document.getElementById("searchName").onkeyup = function (e) {
  if (e.key == "Enter") {
    document.getElementById("btnTimNV").click();
  }
};

document.getElementById("header-title").onclick = function () {
  document.getElementById("tknv").value = getRndInteger(1000, 999999);
  document.getElementById("name").value = "Can Hoang Lam";
  document.getElementById("email").value = "hoanglam0869@gmail.com";
  document.getElementById("password").value = "La^m280693";
  document.getElementById("luongCB").value = `${getRndInteger(100, 2000)}0000`;
  document.getElementById("chucvu").value = position[getRndInteger(0, 2)];
  document.getElementById("gioLam").value = getRndInteger(80, 200);
};

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const position = ["Sếp", "Trưởng phòng", "Nhân viên"];
